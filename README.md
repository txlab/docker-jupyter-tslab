# JupyterLab with [TSLab Kernel](https://github.com/yunabe/tslab)

### Run with docker cli

```bash
docker run --rm -v $(pwd):/home/jovyan -p 8888:8888 registry.gitlab.com/txlab/docker-jupyter-tslab/master:latest
```

### Run with docker-compose
```yaml
version: '3'

services:
    jupyter:
        image: registry.gitlab.com/txlab/docker-jupyter-tslab/master:latest
        # if you want Password login:
        #command: [ "start.sh", 'jupyter', 'lab', "--LabApp.password='HASH'"] # Generate hash with IPython.lib.passwd()
        volumes:
            - .:/home/jovyan
        ports:
            - 8888:8888
```
