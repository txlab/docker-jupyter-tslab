FROM jupyter/base-notebook

ENV JUPYTER_ENABLE_LAB=true

RUN rm -rf /home/jovyan/work && \
  npm install -g yarn tslab && tslab install
  # && jupyter labextension install @jupyterlab/toc

CMD [ "start.sh", "jupyter", "lab" ]
